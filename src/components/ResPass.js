import React from "react";
import { Card, CardActions, CardActionButtons } from 'rmwc/Card';
import { Typography } from 'rmwc/Typography';
import { TextField } from 'rmwc/TextField';
import { Button } from 'rmwc/Button';

class ResPass extends React.Component {
  
    ResPass = e => {
        e.preventDefault();
        const email = this.emailRef.value;
        this.props.ResPass(email);
        e.currentTarget.reset();
    };  
  
  render() {
    return (
        <Card className = "SignForm">
            <form onSubmit={this.ResPass}>
            <div className = "center"><Typography use="headline2">Tweeter</Typography></div>
                <div className = "SignDiv">
                    <TextField box required label="Email" type="email" ref={input => this.emailRef = input}/>
                </div>
                <CardActions className = "SignButtons">
                    <CardActionButtons>
                    <div className = "LinkButtons">
                        <Button dense type="Button" onClick={() => this.props.switchform("SignIn")}>Sign In</Button>
                    </div>
                    <Button raised type="Submit">Reset Password</Button>
                    </CardActionButtons>
                </CardActions>
            </form>
        </Card>
    );
  }
}

export default ResPass;
