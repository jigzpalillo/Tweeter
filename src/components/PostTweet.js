import React from "react";
import { Card, CardAction, CardActions, CardActionButtons } from 'rmwc/Card';
import { TextField } from 'rmwc/TextField';

class PostTweet extends React.Component {

  splitTweet = (tweet, max) => {
    if(tweet.length <= max){
      var tweets=[tweet];
      return tweets;
    } //check if needs splitting
      
    const words = tweet.split(' ');
    var chunks = 1; //assumes total chunks is in single digits (1-9)
    var loop = true; //loop once if chunk digits change
    while(loop){
      var indicator, last, tweets = [""];
      for (const word in words){
        last = tweets.length-1;
        indicator = 2 + (last).toString().length + chunks.toString().length;
        if(( tweets[last] + words[word] ).length < max - indicator)
          tweets[last] = tweets[last] + (tweets[last].length > 0 ? " " : "") + words[word];
        else
          tweets.push( words[word] );
      }
      if((tweets.length).toString().length > chunks.toString().length) //check if chunk digits changed
        loop = true;
      else
        loop = false;
      chunks = tweets.length;
    }
    for (const tweet in tweets){ // add indicators to tweets
      tweets[tweet] = Number(tweet)+1 + "/" + chunks + " " + tweets[tweet];
      //tweets[tweet] = tweets[tweet] + " " + tweets[tweet].length; //for displaying tweet count
    }
    return tweets;
};

postTweet = e => {
  e.preventDefault();
  const tweets = this.splitTweet(this.tweetRef.value.replace(/\s\s+/g, ' '),50);
  this.props.PushTweets(tweets);
  e.currentTarget.reset();
};

    render() {
      return (
        <Card className="posttweet">
          <form onSubmit={this.postTweet}>
            <div style={{ padding: '0 1rem 0rem 1rem' }}>
              <TextField textarea label="Write your Tweet..." className="tweetarea" ref={input => this.tweetRef = input}/>
            </div>
            <CardActions>
              <CardActionButtons>
                <CardAction>Preview</CardAction>
                <CardAction type="Submit">Post Tweet</CardAction>
              </CardActionButtons>
            </CardActions>
          </form>
        </Card> 
      );
    }
  }
  
  export default PostTweet;