import React from "react";
import {
  TopAppBar,
  TopAppBarRow,
  TopAppBarSection,
  TopAppBarNavigationIcon,
  TopAppBarTitle
} from "rmwc/TopAppBar";
import Menudrawer from "./Drawer";

class Navbar extends React.Component {
  render() {
    return (
      <React.Fragment>
        <TopAppBar dense={1} fixed={1} className={"noselect "+(this.props.drawer ? "padded " : "unpadded ")}>
            <TopAppBarRow>
              <TopAppBarSection alignStart>
                <TopAppBarNavigationIcon
                  use="menu"
                  onClick={this.props.toggledrawer}
                />
                <TopAppBarTitle>Tweeter</TopAppBarTitle>
              </TopAppBarSection>
              <TopAppBarSection alignEnd>
                
              </TopAppBarSection>
            </TopAppBarRow>
        </TopAppBar>
        <Menudrawer drawer={this.props.drawer} LogOut={this.props.LogOut} User={this.props.User}/>
      </React.Fragment>
    );
  }
}

export default Navbar;
