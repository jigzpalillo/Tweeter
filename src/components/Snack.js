import React from "react";
import { Snackbar } from 'rmwc/Snackbar';

class Snack extends React.Component {
      render() {
        return (
          <Snackbar
            show={this.props.snackbarIsOpen}
            onHide={evt => this.props.hidesnack()}
            message={this.props.snackMessage}
            actionText="Close"
            actionHandler={() => {}}
        />
        );
      }
    }

export default Snack;
