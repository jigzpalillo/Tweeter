import React from "react";
import {
  Card,
  CardAction,
  CardActions,
  CardActionButtons,
  CardActionIcons
} from 'rmwc/Card';
import { TextField } from 'rmwc/TextField';
import { Typography } from 'rmwc/Typography';

class Feed extends React.Component {

  render() {
    return (
      <div className="feed">
        {Object.keys(this.props.tweets).map(key => (
          <Card key={key}>
            <div style={{ padding: '0 1rem 1rem 1rem' }}>
                <Typography use="headline6" tag="h2">
                  {this.props.tweets[key]}
                </Typography>
                <Typography use="subtitle2" tag="h3" theme="text-secondary-on-background" style={{ marginTop: '-1rem' }}>
                  by {this.props.user.displayName}
                </Typography>
            </div>
          </Card>
        ))}
  
      {this.props.tweets.length===0 &&
        <Card>
          <div style={{ padding: '1rem 1rem 1rem 1rem', textAlign: 'center' }}>
            <Typography use="headline4">No Tweets to Show =(</Typography>
          </div>
        </Card>
      }
      </div>
    );
  }
}

export default Feed;
