import React from "react";
import { Card, CardPrimaryAction, CardAction, CardActions} from 'rmwc/Card';
import { ListDivider } from 'rmwc/List';
import { Icon } from 'rmwc/Icon';
import { Typography } from 'rmwc/Typography';
import PostTweet from './PostTweet';

class ActionMenu extends React.Component {
  render() {
    return (
      <div className="actionmenu">
        <PostTweet PushTweets={this.props.PushTweets}/>
        <Card className="SideMenu">
            <Typography use="subtitle1" tag="div" style={{ padding: '0.5rem 1rem' }} theme="text-secondary-on-background">News Feed</Typography>
                <ListDivider />
            <CardPrimaryAction>
                <div style={{ padding: '1rem' }}>
                <Typography use="headline5" tag="div">
                    Oh backend, why won't you work...
                </Typography>
                <Typography use="body1" tag="p" theme="text-secondary-on-background">
                    by Jigz Palillo
                </Typography>
                </div>
            </CardPrimaryAction>
            <ListDivider />
            <CardActions fullBleed>
                <CardAction>
                Show More <Icon use="arrow_forward" />
                </CardAction>
            </CardActions>
        </Card>
      </div>
    );
  }
}

export default ActionMenu;
