import React from "react";
import { Card, CardActions, CardActionButtons } from 'rmwc/Card';
import { Typography } from 'rmwc/Typography';
import { TextField } from 'rmwc/TextField';
import { Button } from 'rmwc/Button';

class SignUp extends React.Component {

    SignUp = e => {
        e.preventDefault();
        const email = this.emailRef.value;
        const pass = this.passRef.value;
        const name = this.nameRef.value;
        this.props.SignUp(name, email, pass);
        e.currentTarget.reset();
    };  

  render() {
    return (
        <Card className = "SignForm">
            <form onSubmit={this.SignUp}>
            <div className = "center"><Typography use="headline2">Tweeter</Typography></div>
                <div className = "SignDiv">
                    <TextField box required label="Name" ref={input => this.nameRef = input}/>
                    <TextField box required label="Email" type="email" ref={input => this.emailRef = input}/>
                    <TextField box required label="Password" type="password" ref={input => this.passRef = input}/>
                </div>
                <CardActions className = "SignButtons">
                    <CardActionButtons>
                    <div className = "LinkButtons">
                        <Button dense type="Button" onClick={() => this.props.switchform("SignIn")}>Sign In</Button>
                    </div>
                    <Button raised type="Submit">Register</Button>
                    </CardActionButtons>
                </CardActions>
            </form>
        </Card>
    );
  }
}

export default SignUp;
