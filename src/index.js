import React from "react";
import ReactDOM from "react-dom";
import Router from "./components/Router";
import "material-components-web/dist/material-components-web.min.css";
import "./styles/style.css";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<Router />, document.querySelector("#main"));
registerServiceWorker();
